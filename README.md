1. Run cp .env.example .env 
2. Run docker-compose up -d for running the application
3. Go inside the container by running docker-compose exec php bash
4. Run "migration" file: cd database/migrations and php migration_1.php
5. Check project on http://localhost