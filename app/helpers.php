<?php

use JetBrains\PhpStorm\NoReturn;

if (!function_exists('dd')) {
    /**
     * @param $data
     * @return void
     */
    #[NoReturn] function dd($data): void
    {
        echo '<pre>' . var_export($data, true) . '</pre>';
        die(1);
    }
}