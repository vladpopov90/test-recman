<?php

namespace app\Services;

use app\Classes\Auth;
use app\Classes\Dto\UserDto;
use app\Classes\Error;
use app\Repository\UserRepository;

class UserService
{
    /**
     * @var UserRepository $userRepository
     */
    private UserRepository $userRepository;

    /**
     * User service constructor
     */
    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * @param UserDto $dto
     * @return void
     * @throws \Exception
     */
    public function create(UserDto $dto): void
    {
        if ($dto->getPassword() !== $dto->getConfirmPassword()) {
            throw new \Exception('password mismatch');
        }

        $dto->password = md5($dto->getPassword());

        $id = $this->userRepository->create([
            'first_name' => $dto->getFirstName(),
            'last_name' => $dto->getLastName(),
            'email' => $dto->getEmail(),
            'phone_number' => $dto->getPhone(),
            'password' => $dto->getPassword(),
        ]);

        $user = $this->userRepository->find($id)[0] ?? [];

        if (!$user) {
            throw new \Exception('user not found');
        }

        Auth::login($user);
    }

    /**
     * @param array $filters
     * @return array
     */
    public function get(array $filters): array
    {
        return $this->userRepository->findAll($filters);
    }

    /**
     * @param array $filters
     * @return bool
     * @throws \Exception
     */
    public function login(array $filters): bool
    {
        $users = $this->userRepository->findAll($filters);

        if (!count($users)) {
            return false;
        }

        Auth::login($users[0]);
        return true;
    }
}