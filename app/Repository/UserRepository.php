<?php

namespace app\Repository;

use app\Repository\RepositoryInterface;

class UserRepository implements RepositoryInterface
{
    /**
     * @var \PDO $conn
     */
    private \PDO $conn;

    public function __construct()
    {
        $this->conn = new \PDO(
            'mysql:host=db;dbname=recman',
            'root',
            'pwd1234'
        );
    }

    /**
     * @param int $id
     * @return array|false
     */
    public function find(int $id)
    {
        return $this->conn->query("SELECT * FROM users WHERE id = {$id}")->fetchAll();
    }

    /**
     * @param array $filters
     * @return array
     */
    public function findAll(array $filters = []): array
    {
        $whereCondition = '';
        if (count($filters)) {
            $whereCondition = 'WHERE ';
            foreach ($filters as $key => $value) {
                $whereCondition .= "{$key} = '$value' AND ";
            }
            $whereCondition = substr($whereCondition, 0, -4);
        }

        return $this->conn
            ->query("SELECT * FROM users " . $whereCondition)
            ->fetchAll();
    }

    /**
     * @param array $data
     * @return int
     */
    public function create(array $data): int
    {
         $this->conn->prepare(
            "INSERT INTO users (first_name, last_name, phone_number, email, password) VALUES (
                ?, ?, ?, ?, ?
            )")->execute([
            $data['first_name'],
            $data['last_name'],
            $data['phone_number'],
            $data['email'],
            $data['password']
        ]);

         return $this->conn->lastInsertId();
    }

    /**
     * @param int $id
     * @param array $data
     * @return void
     */
    public function update(int $id, array $data)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }
}