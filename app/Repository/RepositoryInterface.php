<?php

namespace app\Repository;

interface RepositoryInterface
{
    public function find(int $id);

    public function findAll(array $filters = []);

    public function create(array $data);

    public function update(int $id, array $data);

    public function delete(int $id);
}