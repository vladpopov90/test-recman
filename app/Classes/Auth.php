<?php

namespace app\Classes;

class Auth
{
    /**
     * @param array $user
     * @return void
     */
    public static function login(array $user)
    {
        $_SESSION['email'] = $user['email'];
        $_SESSION['id'] = $user['id'];
    }

    /**
     * @return void
     */
    public static function logout()
    {
        unset($_SESSION['email']);
        unset($_SESSION['id']);

        session_destroy();
    }

    public static function user(): array
    {
        return [
            'email' => $_SESSION['email'],
            'id' => $_SESSION['id'],
        ];
    }

    /**
     * @return bool
     */
    public static function check(): bool
    {
        return isset($_SESSION['email']) && isset($_SESSION['id']);
    }
}