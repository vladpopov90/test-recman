<?php

namespace app\Classes;

use app\Services\UserService;

class Router
{
    /**
     * @var array
     */
    private static array $routes;

    /**
     * @param string $route
     * @param string $controller
     * @return void
     */
    public static function get(string $route, string $controller): void
    {
        if (!isset(self::$routes)) {
            self::$routes = [];
        }

        self::addRoute('GET', $route, $controller);
    }

    /**
     * @param string $route
     * @param string $controller
     * @return void
     */
    public static function post(string $route, string $controller): void
    {
        if (!isset(self::$routes)) {
            self::$routes = [];
        }

        self::addRoute('POST', $route, $controller);
    }

    /**
     * @param $method
     * @param $route
     * @param $controller
     * @return void
     */
    protected static function addRoute($method, $route, $controller): void
    {
        self::$routes[$method][$route] = $controller;
    }

    /**
     * @param string $method
     * @param string $request
     */
    public static function route(string $method, string $request)
    {
        if (!in_array($method, ['GET', 'POST', 'PUT', 'DELETE'])) {
            echo 'Method not allowed';
            die(0);
        }

        $routes = self::$routes ?? [];

        if (array_key_exists($request, $routes[$method])) {
            $controllerName = $routes[$method][$request];
            $controllerData = explode('@', $controllerName);
            $controller = 'app\\Controllers\\' . $controllerData[0];

            try {
                $reflection = new \ReflectionClass($controller);
                $params = $reflection->getConstructor()?->getParameters() ?? [];
                $arrayParams = [];
                foreach ($params as $param) {
                    $paramClass = $param->getType()->getName();
                    $arrayParams[] = new $paramClass;
                }
            } catch (\Exception $exception) {
                echo $exception->getMessage();
                die(0);
            }

            $controller = new $controller(...$arrayParams);
            $controller->{$controllerData[1]}(
                self::getRequestData($method)
            );
        } else {
            require 'view/404.php';
        }
    }

    /**
     * @param string $method
     * @return Request
     */
    private static function getRequestData(string $method): Request
    {
        if ($method === 'GET') {
            return new Request($_GET);
        }
        if ($method === 'POST' || $method === 'PUT') {
            return new Request($_POST);
        }

        return new Request([]);
    }
}