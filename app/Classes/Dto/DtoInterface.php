<?php

namespace app\Classes\Dto;

interface DtoInterface
{
    /**
     * @return array
     */
    public function toArray(): array;
}