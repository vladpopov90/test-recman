<?php

namespace app\Classes\Dto;

class UserDto extends AbstractDto
{
    private string $firstName;

    private string $lastName;

    private string $email;

    private string $phone;

    private ?string $password;

    private ?string $confirmPassword;

    public function __construct(
        string $firstName,
        string $lastName,
        string $email,
        string $phone,
        ?string $password = null,
        ?string $confirmPassword = null
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->phone = $phone;
        $this->password = $password;
        $this->confirmPassword = $confirmPassword;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getConfirmPassword(): ?string
    {
        return $this->confirmPassword;
    }

    /**
     * @param string $name
     * @param $value
     * @return void
     */
    public function __set(string $name, $value)
    {
        $this->{$name} = $value;
    }
}