<?php

namespace app\Classes\Dto;

abstract class AbstractDto implements DtoInterface
{
    /**
     * @return array
     */
    public function toArray(): array
    {
        $result = [];

        foreach ($this as $key => $value) {
            $result[$key] = $value;
        }

        return $result;
    }
}