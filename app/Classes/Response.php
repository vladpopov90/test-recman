<?php

namespace app\Classes;

class Response
{
    public static function error(array $errors, array $old, string $view): void
    {
        require $view;
    }
}