<?php

namespace app\Classes;

use AllowDynamicProperties;

#[AllowDynamicProperties]
class Request
{
    protected array $data;

    public function __construct(array $data)
    {
        $this->data = [];
        foreach ($data as $key => $value) {
            $this->{$key} = $this->secure($value);
            $this->data[$key] = $this->secure($value);
        }
    }

    public function get(string $name, ?string $default = null): string|null
    {
        if (isset($this->{$name})) {
            return $this->{$name};
        }

        return $default;
    }

    public function __get(string $name)
    {
        return $this->get($name);
    }

    /**
     * @param $value
     * @return string
     */
    private function secure($value): string
    {
        $value = trim($value);
        $value = stripslashes($value);
        return htmlspecialchars($value);
    }

    public function validate(array $rules, string $redirect): bool
    {
        $errors = [];
        $old = [];

        foreach ($this->data as $key => $value) {
            $old[$key] = $value;
            if (!isset($rules[$key])) {
                continue;
            }

            $keyRules = explode('|', $rules[$key]);
            $conn = new \PDO(
                'mysql:host=db;dbname=recman',
                'root',
                'pwd1234'
            );
            foreach ($keyRules as $rule) {
                if ($rule === 'required' && empty($value)) {
                    $errors[$key][] = 'Field is required';
                }

                if ($rule === 'email' && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $errors[$key][] = 'Field must be a valid email';
                }

                if ($rule === 'numeric' && !is_numeric($value)) {
                    $errors[$key][] = 'Field must be a number';
                }

                if (str_contains($rule, 'min')) {
                    $min = explode(':', $rule)[1];
                    if (strlen($value) < $min) {
                        $errors[$key][] = "Field must be at least {$min} characters";
                    }
                }

                if (str_contains($rule, 'same')) {
                    $same = explode(':', $rule)[1];
                    if ($value !== $this->data[$same]) {
                        $errors[$key][] = "Field must be the same as {$same}";
                    }
                }

                if (str_contains($rule, 'unique')) {
                    $unique = explode(':', $rule)[1];
                    $table = explode(',', $unique)[0];
                    $column = explode(',', $unique)[1];
                    $count = 0;
                    $dbData = $conn
                        ->query("SELECT * FROM {$table} WHERE {$column} = '$value'")
                        ->fetchAll();
                    $count += count($dbData);

                    if ($count) {
                        $errors[$key][] = "Field must be unique";
                    }
                }
            }
        }

        if (count($errors)) {
            require $redirect;
            return false;
        }

        return true;
    }
}