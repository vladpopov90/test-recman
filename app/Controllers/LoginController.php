<?php

namespace app\Controllers;

use app\Classes\Auth;
use app\Classes\Request;
use app\Classes\Response;
use app\Services\UserService;

class LoginController
{
    /**
     * @var UserService $userService
     */
    private UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;

        if (Auth::check()) {
            header('Location: /profile');
            return;
        }
    }

    /**
     * @return void
     */
    public function index()
    {
        require 'view/login.php';
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function login(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8'
        ], 'view/login.php');

        if (!$validated) {
            return;
        }

        $success = $this->userService->login([
            'email' => $request->get('email'),
            'password' => md5($request->get('password'))
        ]);

        if ($success) {
            header('Location: /profile');
            return;
        }

        Response::error(
            [
                'user' => ['Invalid email or password'],
            ],
            [
                'email' => $request->get('email')
            ],
            'view/login.php'
        );
    }
}