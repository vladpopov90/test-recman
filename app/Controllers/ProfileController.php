<?php

namespace app\Controllers;

use app\Classes\Auth;
use app\Classes\Request;
use app\Services\UserService;

class ProfileController
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        if (!Auth::check()) {
            header('Location: /login');
        }

        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $users = $this->userService->get([
            'id' => $_SESSION['id'],
        ]);

        if (!count($users)) {
            header('Location: /login');
        }

        $user = $users[0];

        require 'view/profile.php';
    }
}