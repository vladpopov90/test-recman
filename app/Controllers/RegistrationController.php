<?php

namespace app\Controllers;

use app\Classes\Auth;
use app\Classes\Dto\UserDto;
use app\Classes\Request;
use app\Services\UserService;

class RegistrationController
{
    private UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;

        if (Auth::check()) {
            header('Location: /profile');
            return;
        }
    }

    /**
     * @return void
     */
    public function index()
    {
        require 'view/register.php';
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Exception
     */
    public function register(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email|unique:users,email',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required|numeric|unique:users,phone_number',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password'
        ], 'view/register.php');

        if (!$validated) {
            return;
        }

        $this->userService->create(
            new UserDto(
                $request->get('first_name'),
                $request->get('last_name'),
                $request->get('email'),
                $request->get('phone_number'),
                $request->get('password'),
                $request->get('password_confirmation')
            )
        );

        header('Location: /profile');
    }
}