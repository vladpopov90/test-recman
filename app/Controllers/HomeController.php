<?php

namespace app\Controllers;

use app\Classes\Auth;

class HomeController
{
    public function index()
    {
        if (Auth::check()) {
            header('Location: /profile');
        }

        require 'view/home.php';
    }
}