<?php

namespace app\Controllers;

use app\Classes\Auth;

class LogoutController
{
    public function index()
    {
        Auth::logout();
        header('Location: /login');
    }
}