<?php

use app\Classes\Router;

/*
 * Default namespace for controllers is app\Controllers
 */
Router::get('/', 'HomeController@index');
Router::get('/registration', 'RegistrationController@index');
Router::post('/registration', 'RegistrationController@register');
Router::get('/login', 'LoginController@index');
Router::post('/login', 'LoginController@login');
Router::get('/profile', 'ProfileController@index');
Router::get('/logout', 'LogoutController@index');