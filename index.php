<?php

require_once 'app/helpers.php';
require_once 'app/Classes/Router.php';
require_once 'app/Classes/Request.php';
require_once 'app/Classes/Dto/DtoInterface.php';
require_once 'app/Classes/Dto/AbstractDto.php';
require_once 'app/Classes/Dto/UserDto.php';
require_once 'app/Classes/Auth.php';
require_once 'app/Classes/Response.php';
require_once 'app/Controllers/RegistrationController.php';
require_once 'app/Controllers/HomeController.php';
require_once 'app/Controllers/LoginController.php';
require_once 'app/Controllers/ProfileController.php';
require_once 'app/Controllers/LogoutController.php';
require_once 'app/Services/UserService.php';
require_once 'app/Repository/RepositoryInterface.php';
require_once 'app/Repository/UserRepository.php';
require_once 'routes/routes.php';

use app\Classes\Router;

session_start();

Router::route(
    $_SERVER['REQUEST_METHOD'],
    $_SERVER['REQUEST_URI']
);

