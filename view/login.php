<?php

?>

<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <script>
        tailwind.config = {
            theme: {
                extend: {
                    colors: {
                        clifford: '#da373d',
                    }
                }
            }
        }
    </script>
</head>
<body>
<div class="w-full max-w-xs mx-auto mt-12">
    <form method="POST" action="/login" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                Email
            </label>
            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none
                    focus:shadow-outline <?php if (isset($errors['email'])) echo 'border-red-500' ?>"
                   required
                   name="email" id="email" type="email" placeholder="Email"
                   value="<?php if (isset($old['email'])) echo $old['email'] ?>"
            >
            <?php if (isset($errors['email']))
                echo "<p class='text-red-500 text-xs italic'> {$errors['email'][0]} </p>";
            ?>
        </div>
        <div class="mb-6">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                Password
            </label>
            <input required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight
                    focus:outline-none focus:shadow-outline <?php if (isset($errors['password'])) echo 'border-red-500' ?>"
                   name="password" id="password" type="password" placeholder="******************"
                   value="<?php if (isset($old['password'])) echo $old['password'] ?>"
            >
            <?php if (isset($errors['password']))
                echo "<p class='text-red-500 text-xs italic'> {$errors['password'][0]} </p>";
            ?>
        </div>
        <div class="mb-6">
            <?php if (isset($errors['user']))
                echo "<p class='text-red-500 text-xs italic'> {$errors['user'][0]} </p>";
            ?>
        </div>
        <div class="flex items-center justify-between">
            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    type="submit">
                Login
            </button>
            <a class="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800"
               href="/">
                Home page
            </a>
        </div>
    </form>
    <p class="text-center text-gray-500 text-xs">
        &copy;2020 Acme Corp. All rights reserved.
    </p>
</div>
</body>
</html>