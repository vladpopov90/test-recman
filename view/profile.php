<?php

?>

<!DOCTYPE html>
<html>
<head>
    <title>Profile</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <script>
        tailwind.config = {
            theme: {
                extend: {
                    colors: {
                        clifford: '#da373d',
                    }
                }
            }
        }
    </script>
</head>
<body>
    <div class="w-full max-w-xs mx-auto mt-12">
        <div>
            <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" href="/logout">Logout</a>
        </div>
        <h1 class="mt-12 h1">Profile</h1>
        <div class="relative overflow-x-auto">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <tbody>
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-400 hover:text-black">
                    <td class="border border-slate-600 px-6 py-4">First name</td>
                    <td class="border border-slate-600 px-6 py-4"><?= $user['first_name'] ?></td>
                </tr>
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-400 hover:text-black">
                    <td class="border border-slate-600 px-6 py-4">Last name</td>
                    <td class="border border-slate-600 px-6 py-4"><?= $user['last_name'] ?></td>
                </tr>
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-400 hover:text-black">
                    <td class="border border-slate-600 px-6 py-4">Email</td>
                    <td class="border border-slate-600 px-6 py-4"><?= $user['email'] ?></td>
                </tr>
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-400 hover:text-black">
                    <td class="border border-slate-600 px-6 py-4">Phone number</td>
                    <td class="border border-slate-600 px-6 py-4"><?= $user['phone_number'] ?></td>
                </tr>
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-400 hover:text-black">
                    <td class="border border-slate-600 px-6 py-4">Created</td>
                    <td class="border border-slate-600 px-6 py-4"><?= $user['created_at'] ?></td>
                </tr>
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-400 hover:text-black">
                    <td class="border border-slate-600 px-6 py-4">Updated</td>
                    <td class="border border-slate-600 px-6 py-4"><?= $user['updated_at'] ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>